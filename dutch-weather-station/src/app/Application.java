package app;

import app.controller.Controller;
import app.domain.Data;
import app.gateway.MessageSenderGateway;
import com.google.gson.Gson;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Application extends javafx.application.Application {
  private final String HOST = "localhost";
  private final static String PUBLISH_CHANNEL = "dutch-weather-station-channel";
  private static final String TITLE = "Dutch weather station";

  private MessageSenderGateway senderGateway;
  private Controller controller;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws Exception{
    FXMLLoader loader = new FXMLLoader(getClass().getResource("view/view.fxml"));
    Pane root = loader.load();
    Scene scene = new Scene(root);

    stage.setTitle(TITLE);
    stage.setOnCloseRequest(event -> onApplicationShutDown());
    stage.setScene(scene);
    stage.show();

    controller = loader.getController();
    controller.setApplication(this);
    controller.measureData();

    senderGateway = new MessageSenderGateway(HOST, PUBLISH_CHANNEL);
  }

  public void publishData(Data data) {
    try {
      String json = new Gson().toJson(data);
      senderGateway.publishToChannel(json);
      controller.appendDataToLog(data);
    } catch (Exception e) {
      e.printStackTrace();

      controller.setErrorToLog(e);
    }
  }

  private void onApplicationShutDown() {
    try {
      senderGateway.close();
    } catch (Exception e) {
      e.printStackTrace();

      controller.setErrorToLog(e);
    }
  }

}
