package app.controller;

import app.Application;
import app.domain.Data;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Controller implements Initializable {
  private Application application;
  private ObservableList<Data> measuredDataList = FXCollections.observableArrayList();
  private ObservableList<String> logList = FXCollections.observableArrayList();

  @FXML
  private ListView<Data> lvMeasuredData;

  @FXML
  private ListView<String> lvLog;

  public Controller () {
    // Empty
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    lvMeasuredData.setCellFactory(param -> new ListCell<Data>() {
      @Override
      protected void updateItem(Data data, boolean bln) {
        super.updateItem(data, bln);

        if (data != null) {
          Platform.runLater(() -> {
            StringBuilder stringBuilder = new StringBuilder("");
            stringBuilder.append("Country: ");
            stringBuilder.append(data.getCountry());
            stringBuilder.append("\t");
            stringBuilder.append("Temperature: ");
            stringBuilder.append(data.getTemperature());
            stringBuilder.append("\t");
            stringBuilder.append("Humidity: ");
            stringBuilder.append(data.getHumidity());
            stringBuilder.append("\t");
            stringBuilder.append("DateTime: ");
            stringBuilder.append(data.getDateTime().toString());

            // Append data in string format
            setText(stringBuilder.toString());

            // Scroll to bottom of list
//            lvMeasuredData.scrollTo(measuredDataList.size() - 1);
          });
        }
      }
    });

    lvMeasuredData.setItems(measuredDataList);

    lvLog.setItems(logList);
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public void measureData() {
    Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
      int temperature = new Random().nextInt(10) + 20;
      int humidity = new Random().nextInt(10) + 15;

      Data data = new Data(temperature, humidity);

      Platform.runLater(() -> {
        application.publishData(data);
        measuredDataList.add(data);
      });

    }, 0, 2, TimeUnit.SECONDS);
  }

  public void appendDataToLog(Data data) {
    StringBuilder stringBuilder = new StringBuilder("");

    stringBuilder.append("Published:\t");
    stringBuilder.append("Country: ");
    stringBuilder.append(data.getCountry());
    stringBuilder.append("\t");
    stringBuilder.append("Temperature: ");
    stringBuilder.append(data.getTemperature());
    stringBuilder.append("\t");
    stringBuilder.append("Humidity: ");
    stringBuilder.append(data.getHumidity());
    stringBuilder.append("\t");
    stringBuilder.append("DateTime: ");
    stringBuilder.append(data.getDateTime());

    Platform.runLater(() -> {
      logList.add(stringBuilder.toString());
    });
  }

  public void setErrorToLog(Exception exception) {
    StringBuilder stringBuilder = new StringBuilder("");

    stringBuilder.append("Error: ");
    stringBuilder.append(exception.getMessage());
    stringBuilder.append("\n");

    Platform.runLater(() -> {
      logList.add(stringBuilder.toString());
    });
  }
}
