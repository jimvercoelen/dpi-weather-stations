package app.gateway;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class MessageSenderGateway {
  private final String host;
  private final String queueChannel;
  private final Connection connection;
  private final Channel channel;

  public MessageSenderGateway(String host, String queueChannel) throws Exception {
    this.host = host;
    this.queueChannel = queueChannel;

    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(host);
    connection = factory.newConnection();

    channel = connection.createChannel();
    channel.queueDeclare(queueChannel, false, false, false, null);
  }

  public void publishToChannel(String json) throws Exception {
    channel.basicPublish("", queueChannel, null, json.getBytes());
  }

  public void close() throws Exception {
    channel.close();
    connection.close();
  }
}
