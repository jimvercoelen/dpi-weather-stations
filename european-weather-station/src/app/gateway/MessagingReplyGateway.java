package app.gateway;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MessagingReplyGateway {
  private final String host;
  private final String queueChannel;
  private final Connection connection;
  private final Channel channel;

  public MessagingReplyGateway(String host, String queueName) throws IOException, TimeoutException {
    this.host = host;
    this.queueChannel = queueName;

    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(host);

    connection = factory.newConnection();
    channel = connection.createChannel();

    channel.queueDeclare(queueName, false, false, false, null);
    channel.basicQos(1);
  }

  public Channel getChannel() {
    return channel;
  }

  public void setConsumer(Consumer consumer) throws IOException {
    channel.basicConsume(queueChannel, false, consumer);
  }

  public void close() throws IOException, TimeoutException {
    channel.close();
  }
}
