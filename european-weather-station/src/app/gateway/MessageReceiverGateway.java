package app.gateway;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;

public class MessageReceiverGateway {
  private final String host;
  private final String queueChannel;
  private final Connection connection;
  private final Channel channel;

  public MessageReceiverGateway(String host, String queueChannel) throws Exception {
    this.queueChannel = queueChannel;
    this.host = host;

    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(host);
    connection = factory.newConnection();

    channel = connection.createChannel();
    channel.queueDeclare(queueChannel, false, false, false, null);
  }

  public Channel getChannel() {
    return channel;
  }

  public void setConsumer(DefaultConsumer consumer) throws Exception {
    channel.basicConsume(queueChannel, true, consumer);
  }

  public void close() throws Exception {
    channel.close();
    connection.close();
  }
}
