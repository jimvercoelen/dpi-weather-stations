package app.domain;

public class Data {
  private String country;
  private int temperature;
  private int humidity;
  private String dateTime;

  public Data() {

  }

  public Data(int temperature, int humidity) {
    this.temperature = temperature;
    this.humidity = humidity;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public int getTemperature() {
    return temperature;
  }

  public void setTemperature(int temperature) {
    this.temperature = temperature;
  }

  public int getHumidity() {
    return humidity;
  }

  public void setHumidity(int humidity) {
    this.humidity = humidity;
  }

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }

  @Override
  public String toString() {
    return "Country: " + country +
      " temperature: " + temperature
      + " humidity: " + humidity
      + "dateTime: " + dateTime;
  }
}
