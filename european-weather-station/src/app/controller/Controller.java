package app.controller;

import app.Application;
import app.domain.Data;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
  private Application application;
  private ObservableList<Data> incomingDataList = FXCollections.observableArrayList();
  private ObservableList<String> requestsResponsesList = FXCollections.observableArrayList();
  private ObservableList<String> logList = FXCollections.observableArrayList();

  @FXML
  private ListView<Data> lvIncomingData;

  @FXML
  private ListView<String> lvRequestsResponses;

  @FXML
  private ListView<String> lvLog;

  public Controller () {
    // Empty
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    lvIncomingData.setCellFactory(param -> new ListCell<Data>() {
      @Override
      protected void updateItem(Data data, boolean bln) {
        super.updateItem(data, bln);

        if (data != null) {
          Platform.runLater(() -> {
            StringBuilder stringBuilder = new StringBuilder("");
            stringBuilder.append("Country: ");
            stringBuilder.append(data.getCountry());
            stringBuilder.append("\t");
            stringBuilder.append("Temperature: ");
            stringBuilder.append(data.getTemperature());
            stringBuilder.append("\t");
            stringBuilder.append("Humidity: ");
            stringBuilder.append(data.getHumidity());
            stringBuilder.append("\t");
            stringBuilder.append("DateTime: ");
            stringBuilder.append(data.getDateTime());

            // Append data in string format
            setText(stringBuilder.toString());
          });
        }
      }
    });

    lvIncomingData.setItems(incomingDataList);
    lvRequestsResponses.setItems(requestsResponsesList);
    lvLog.setItems(logList);
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public void appendDataToIncomingDataList(Data data) {
    Platform.runLater(() -> {
      incomingDataList.add(data);
    });
  }

  public void appendRequestToList(String json) {
    StringBuilder stringBuilder = new StringBuilder("");

    stringBuilder.append("Request: ");
    stringBuilder.append(json);
    stringBuilder.append("\n");

    Platform.runLater(() -> {
      requestsResponsesList.add(stringBuilder.toString());
    });
  }

  public void appendResponseToList(String json) {
    StringBuilder stringBuilder = new StringBuilder("");

    stringBuilder.append("Response: ");
    stringBuilder.append(json);
    stringBuilder.append("\n");

    Platform.runLater(() -> {
      requestsResponsesList.add(stringBuilder.toString());
    });
  }

  public void appendErrorToLog(Exception exception) {
    StringBuilder stringBuilder = new StringBuilder("");

    stringBuilder.append("Error: ");
    stringBuilder.append(exception.getMessage());
    stringBuilder.append("\n");

    Platform.runLater(() -> {
      logList.add(stringBuilder.toString());
    });
  }
}
