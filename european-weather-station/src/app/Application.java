package app;

import app.controller.Controller;
import app.domain.Data;
import app.domain.RequestFilters;
import app.domain.RequestReply;
import app.gateway.MessageReceiverGateway;
import app.gateway.MessagingReplyGateway;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.sun.tools.javac.util.List;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Application extends javafx.application.Application {
  private static final String TITLE = "European weather station";
  private final String HOST = "localhost";
  private final String EUROPEAN_REQUEST_CHANNEL = "european-request-weather-channel";
  private final String[] WEATHER_STATION_SUBSCRIBE_CHANNELS = {
    "german-weather-station-channel",
    "dutch-weather-station-channel",
    "belgium-weather-station-channel"
  };

  private Controller controller;
  private MessagingReplyGateway clientRequestGateway;
  private HashMap<String, MessageReceiverGateway> receiverGateways = new HashMap<>();
  private ArrayList<Data> dataList = new ArrayList<>();

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws IOException {
    FXMLLoader loader = new FXMLLoader(getClass().getResource("view/view.fxml"));
    Pane root = loader.load();
    Scene scene = new Scene(root);

    stage.setTitle(TITLE);
    stage.setOnCloseRequest(event -> onApplicationShutDown());
    stage.setScene(scene);
    stage.show();

    controller = loader.getController();
    controller.setApplication(this);

    try {
      initializeGateways();
    } catch (Exception e) {
      e.printStackTrace();

      controller.appendErrorToLog(e);
    }
  }

  private void initializeGateways () throws Exception {
    // Initialize receiving gateways for each WEATHER_STATION_SUBSCRIBE_CHANNELS
    for (String channel : WEATHER_STATION_SUBSCRIBE_CHANNELS) {
      MessageReceiverGateway receiverGateway = new MessageReceiverGateway(HOST, channel);
      receiverGateway.setConsumer(new DefaultConsumer(receiverGateway.getChannel()) {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
          String json = new String(body, "UTF-8");
          Data data = new Gson().fromJson(json, Data.class);
          dataList.add(data);
          controller.appendDataToIncomingDataList(data);
        }
      });

      receiverGateways.put(channel, receiverGateway);
    }

    // Initialize reply gateway for EUROPEAN_REQUEST_CHANNEL
    clientRequestGateway = new MessagingReplyGateway(HOST, EUROPEAN_REQUEST_CHANNEL);
    Consumer consumer = new DefaultConsumer(clientRequestGateway.getChannel()) {
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        AMQP.BasicProperties replyProps = new AMQP.BasicProperties
          .Builder()
          .correlationId(properties.getCorrelationId())
          .build();

        String requestJson = new String(body, "UTF-8");
        RequestReply request = new Gson().fromJson(requestJson, RequestReply.class);
        request.setResponseData(getFilteredData(request.getRequestFilters()));
        String responseJson = new Gson().toJson(request, RequestReply.class);

        controller.appendRequestToList(requestJson);
        controller.appendResponseToList(responseJson);

        clientRequestGateway.getChannel().basicPublish( "", properties.getReplyTo(), replyProps, responseJson.getBytes("UTF-8"));
        clientRequestGateway.getChannel().basicAck(envelope.getDeliveryTag(), false);

        // RabbitMq consumer worker thread notifies the RPC server owner thread
        synchronized(this) {
          this.notify();
        }
      }
    };

    clientRequestGateway.setConsumer(consumer);

//    // Wait and be prepared to consume the message from RPC client.
//    while (true) {
//      synchronized(consumer) {
//        try {
//          consumer.wait();
//        } catch (InterruptedException e) {
//          e.printStackTrace();
//        }
//      }
//    }
  }

  private ArrayList<Data> getFilteredData(RequestFilters filters) {
    ArrayList<Data> returnList = new ArrayList<>();

    for (Data data: dataList) {
      if (filters.getFromDate() != null && filters.getTillDate() != null) {
        boolean fromDateIsBeforeDataDate = new DateTime(filters.getFromDate()).isBefore(new DateTime(data.getDateTime()));
        boolean tillDateIsAfterToday = new DateTime(filters.getTillDate()).isAfter(new DateTime(data.getDateTime()));

        if (fromDateIsBeforeDataDate && tillDateIsAfterToday && getDataCountryIsInRequestFilter(data, filters)) {
          returnList.add(data);
        }
      } else if (filters.getFromDate() != null) {
        boolean fromDateIsBeforeDataDate = new DateTime(filters.getFromDate()).isBefore(new DateTime(data.getDateTime()));

        if (fromDateIsBeforeDataDate && getDataCountryIsInRequestFilter(data, filters)) {
          returnList.add(data);
        }
      } else if (filters.getTillDate() != null) {
        boolean tillDateIsAfterToday = new DateTime(filters.getTillDate()).isAfter(new DateTime(data.getDateTime()));

        if (tillDateIsAfterToday && getDataCountryIsInRequestFilter(data, filters)) {
          returnList.add(data);
        }
      } else {
        if (getDataCountryIsInRequestFilter(data, filters)) {
          returnList.add(data);
        }
      }
    }

    return returnList;
  }

  private boolean getDataCountryIsInRequestFilter(Data data, RequestFilters filters) {
    return data.getCountry().equals("Germany") && filters.isGetGermany() ||
      data.getCountry().equals("the Netherlands") && filters.isGetTheNetherlands() ||
      data.getCountry().equals("Belgium") && filters.isGetBelgium();

  }

  private void onApplicationShutDown() {
    try {
      clientRequestGateway.close();

      for (Map.Entry<String, MessageReceiverGateway> item: receiverGateways.entrySet()) {
        item.getValue().close();
      }
    } catch (Exception e) {
      e.printStackTrace();
      controller.appendErrorToLog(e);
    }
  }
}
