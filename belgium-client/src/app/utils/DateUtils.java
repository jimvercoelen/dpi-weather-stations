package app.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.time.LocalDate;

public class DateUtils {

  /**
   * Convert {@link java.time.LocalDate} to {@link org.joda.time.DateTime}
   */
  public static DateTime toDateTime(LocalDate localDate) {
    return new DateTime(DateTimeZone.UTC).withDate(
      localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()
    ).withTime(0, 0, 0, 0);
  }

  /**
   * Convert {@link org.joda.time.DateTime} to {@link java.time.LocalDate}
   */
  public static LocalDate toLocalDate(DateTime dateTime) {
    DateTime dateTimeUtc = dateTime.withZone(DateTimeZone.UTC);
    return LocalDate.of(dateTimeUtc.getYear(), dateTimeUtc.getMonthOfYear(), dateTimeUtc.getDayOfMonth());
  }
}
