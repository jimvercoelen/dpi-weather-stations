package app.controller;

import app.domain.Data;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class ChartController {
  private ObservableList<XYChart.Data<String, Integer>> xyObservableTemperatureList = FXCollections.observableArrayList();
  private ObservableList<XYChart.Data<String, Integer>> xyObservableHumidityList = FXCollections.observableArrayList();
  private CategoryAxis xAxis;
  private NumberAxis yAxis;
  private XYChart.Series temperatureSeries;
  private XYChart.Series humiditySeries;
  private LineChart<String, Number> lineChart;
  private DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("mm:ss");

  public ChartController(String title) {
    xAxis = new CategoryAxis();
    yAxis = new NumberAxis();

    xAxis.setLabel("Time");
    yAxis.setLabel("Value");

    temperatureSeries = new XYChart.Series(xyObservableTemperatureList);
    humiditySeries = new XYChart.Series(xyObservableHumidityList);

    temperatureSeries.setName("Temperature");
    humiditySeries.setName("Humidity");

    lineChart = new LineChart<>(xAxis, yAxis);
    lineChart.setAnimated(false);
    lineChart.setTitle(title);
    lineChart.getData().setAll(temperatureSeries, humiditySeries);
  }

  public LineChart<String, Number> getLineChart() {
    return lineChart;
  }

  public void addDate(Data data) {
    Platform.runLater(() -> {
      String dateTimeString = dateTimeFormatter.print(new DateTime(data.getDateTime()));

      XYChart.Data temperatureData = new XYChart.Data(dateTimeString, data.getTemperature());
      XYChart.Data humidityData = new XYChart.Data(dateTimeString, data.getHumidity());

      if (!xyObservableTemperatureList.contains(temperatureData)) {
        xyObservableTemperatureList.add(temperatureData);
      }

      if (!xyObservableHumidityList.contains(humidityData)) {
        xyObservableHumidityList.add(humidityData);
      }

      // Size should be equal with temperature and humidity
      int size = xyObservableTemperatureList.size();
      if (size > 10) {
        xyObservableTemperatureList.remove(0, 1);
        xyObservableHumidityList.remove(0, 1);
      }
    });
  }
}
