package app.controller;

import app.domain.Data;
import app.domain.RequestReply;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

public class ChartsController {
  private ObservableList<XYChart.Data<String, Integer>> xyListGermanyTemperature = FXCollections.observableArrayList();
  private ObservableList<XYChart.Data<String, Integer>> xyListGermanyHumidity = FXCollections.observableArrayList();
  private ObservableList<XYChart.Data<String, Integer>> xyListDutchTemperature = FXCollections.observableArrayList();
  private ObservableList<XYChart.Data<String, Integer>> xyListDutchHumidity = FXCollections.observableArrayList();
  private ObservableList<XYChart.Data<String, Integer>> xyListBelgiumTemperature = FXCollections.observableArrayList();
  private ObservableList<XYChart.Data<String, Integer>> xyListBelgiumHumidity = FXCollections.observableArrayList();

  private XYChart.Series germanyTemperatureSeries;
  private XYChart.Series germanyHumiditySeries;
  private XYChart.Series dutchTemperatureSeries;
  private XYChart.Series dutchHumiditySeries;
  private XYChart.Series belgiumTemperatureSeries;
  private XYChart.Series belgiumHumiditySeries;

  private CategoryAxis xAxis;
  private NumberAxis yAxis;

  private LineChart<String, Number> lineChart;
  private DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("MM/dd HH:mm:ss");

  public ChartsController() {
    xAxis = new CategoryAxis();
    yAxis = new NumberAxis();

    xAxis.setLabel("Time");
    yAxis.setLabel("Value");

    germanyTemperatureSeries = new XYChart.Series(xyListGermanyTemperature);
    germanyHumiditySeries = new XYChart.Series(xyListGermanyHumidity);
    dutchTemperatureSeries = new XYChart.Series(xyListDutchTemperature);
    dutchHumiditySeries = new XYChart.Series(xyListDutchHumidity);
    belgiumTemperatureSeries = new XYChart.Series(xyListBelgiumTemperature);
    belgiumHumiditySeries = new XYChart.Series(xyListBelgiumHumidity);

    germanyTemperatureSeries.setName("German temperature");
    germanyHumiditySeries.setName("German humidity");

    dutchTemperatureSeries.setName("Dutch temperature");
    dutchHumiditySeries.setName("Dutch humidity");

    belgiumTemperatureSeries.setName("Belgium temperature");
    belgiumHumiditySeries.setName("Belgium humidity");

    lineChart = new LineChart<>(xAxis, yAxis);
    lineChart.setAnimated(false);
    lineChart.setTitle("Weather data");
    lineChart.getData().setAll(
      germanyTemperatureSeries, germanyHumiditySeries,
      dutchTemperatureSeries, dutchHumiditySeries,
      belgiumTemperatureSeries, belgiumHumiditySeries
    );
  }

  public LineChart<String, Number> getLineChart() {
    return lineChart;
  }

  public void addData(RequestReply reply) {
    Platform.runLater(() -> {

      for (Data data: reply.getResponseData()) {
        String dateTimeString = dateTimeFormatter.print(new DateTime(data.getDateTime()));
        XYChart.Data temperatureData = new XYChart.Data(dateTimeString, data.getTemperature());
        XYChart.Data humidityData = new XYChart.Data(dateTimeString, data.getHumidity());

        if (data.getCountry().equals("Germany") &&
          !xyListGermanyTemperature.contains(temperatureData) &&
          !xyListGermanyHumidity.contains(humidityData)
          ) {
          xyListGermanyTemperature.add(temperatureData);
          xyListGermanyHumidity.add(humidityData);

          cutXYList(xyListGermanyTemperature);
          cutXYList(xyListGermanyHumidity);
        } else if (data.getCountry().equals("the Netherlands") &&
          !xyListDutchTemperature.contains(temperatureData) &&
          !xyListDutchHumidity.contains(humidityData)
          ) {
          xyListDutchTemperature.add(temperatureData);
          xyListDutchHumidity.add(humidityData);

          cutXYList(xyListDutchTemperature);
          cutXYList(xyListDutchHumidity);
        } else if (data.getCountry().equals("Belgium") &&
          !xyListBelgiumTemperature.contains(temperatureData) &&
          !xyListBelgiumHumidity.contains(humidityData)
          ) {
          xyListBelgiumTemperature.add(temperatureData);
          xyListBelgiumHumidity.add(humidityData);

          cutXYList(xyListBelgiumTemperature);
          cutXYList(xyListBelgiumHumidity);
        }
      }
    });
  }

  public void cutXYList(ObservableList<XYChart.Data<String, Integer>> list) {
    if (list.size() > 10) {
      list.remove(0, 1);
    }
  }
}
