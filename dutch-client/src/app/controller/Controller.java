package app.controller;

import app.Application;
import app.domain.Data;
import app.domain.RequestFilters;
import app.domain.RequestReply;
import app.utils.DateUtils;
import com.google.gson.Gson;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Controller implements Initializable {
  private Application application;
  private ScheduledExecutorService scheduledExecutorService;
  private ObservableList<Data> incomingDataList = FXCollections.observableArrayList();
  private ObservableList<String> logList = FXCollections.observableArrayList();
  private ChartController germanChartController = new ChartController("Germany");
  private ChartController dutchChartController = new ChartController("the Netherlands");
  private ChartController belgiumChartController = new ChartController("Belgium");
  private ChartsController chartsController = new ChartsController();

  @FXML
  private ListView<Data> lvIncomingData;

  @FXML
  private ListView<String> lvLog;

  @FXML
  private CheckBox cbGermany;

  @FXML
  private CheckBox cbTheNetherlands;

  @FXML
  private CheckBox cbBelgium;

  @FXML
  private DatePicker dpDateFrom;

  @FXML
  private DatePicker dpDateTill;

  @FXML
  private Slider slSchedule;

  @FXML
  private Label lblSchedule;

  @FXML
  private GridPane gpCharts;

  @FXML
  private void onRequestDataClick(ActionEvent event) {
    Double interval = slSchedule.getValue();
    int initialDelay = 0;
    int period = interval == 0 ? 1 : interval.intValue();
    TimeUnit timeUnit = interval == 0 ? TimeUnit.SECONDS : TimeUnit.MINUTES;

    if (scheduledExecutorService != null) {
      scheduledExecutorService.shutdown();
    }

    scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    scheduledExecutorService.scheduleAtFixedRate(() -> {
      boolean getGermany = cbGermany.isSelected();
      boolean getTheNetherlands = cbTheNetherlands.isSelected();
      boolean getBelgium = cbBelgium.isSelected();

      String fromDate = dpDateFrom.getValue() != null ? DateUtils.toDateTime(dpDateFrom.getValue()).toString() : null;
      String tillDate = dpDateTill.getValue() != null ? DateUtils.toDateTime(dpDateTill.getValue()).toString() : null;

      RequestReply request = new RequestReply();
      request.setCorrId(UUID.randomUUID().toString());
      request.setRequestFilters(new RequestFilters(getGermany, getTheNetherlands, getBelgium, fromDate, tillDate));

      appendRequestToLog(new Gson().toJson(request, RequestReply.class));

      try {
        String json = application.requestData(request);
        RequestReply reply = new Gson().fromJson(json, RequestReply.class);
        chartsController.addData(reply);

        divideIncomingDateIntoLists(reply);
        appendResponseToLog(json);
      } catch (Exception e) {
        e.printStackTrace();

        scheduledExecutorService.shutdown();

        appendErrorToLog(e);
      }

    }, initialDelay, period, timeUnit);
  }

  public Controller () {
    // Empty
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {

    slSchedule.valueProperty().addListener((observable, oldValue, newValue) -> {
      int roundedNewValue = newValue.intValue();

      slSchedule.setValue(roundedNewValue);

      if (!slSchedule.isValueChanging()
        || newValue.doubleValue() == slSchedule.getMin()
        || newValue.doubleValue() == slSchedule.getMax()) {

        if (newValue.doubleValue() == slSchedule.getMin()) {
          lblSchedule.setText("continuously");
        } else {
          lblSchedule.setText("every " + roundedNewValue + " minute");
        }
      }
    });

    lvIncomingData.setCellFactory(param -> new ListCell<Data>() {
      @Override
      protected void updateItem(Data data, boolean bln) {
        super.updateItem(data, bln);

        if (data != null) {
          Platform.runLater(() -> {
            StringBuilder stringBuilder = new StringBuilder("");
            stringBuilder.append("Country: ");
            stringBuilder.append(data.getCountry());
            stringBuilder.append("\t");
            stringBuilder.append("Temperature: ");
            stringBuilder.append(data.getTemperature());
            stringBuilder.append("\t");
            stringBuilder.append("Humidity: ");
            stringBuilder.append(data.getHumidity());
            stringBuilder.append("\t");
            stringBuilder.append("DateTime: ");
            stringBuilder.append(data.getDateTime());

            // Append data in string format
            setText(stringBuilder.toString());
          });
        }
      }
    });

    lvIncomingData.setItems(incomingDataList);

    lvLog.setItems(logList);

    gpCharts.getColumnConstraints().remove(0, 1);
//    gpCharts.add(chartsController.getLineChart(), 0, 0);
    gpCharts.add(germanChartController.getLineChart(), 0, 0);
    gpCharts.add(dutchChartController.getLineChart(), 0, 1);
    gpCharts.add(belgiumChartController.getLineChart(), 0, 2);
  }


  public void appendRequestToLog(String json) {
    StringBuilder stringBuilder = new StringBuilder("");

    stringBuilder.append("Request: ");
    stringBuilder.append(json);
    stringBuilder.append("\n");

    Platform.runLater(() -> {
      logList.add(stringBuilder.toString());
    });
  }

  public void appendResponseToLog(String json) {
    StringBuilder stringBuilder = new StringBuilder("");

    stringBuilder.append("Response: ");
    stringBuilder.append(json);
    stringBuilder.append("\n");

    Platform.runLater(() -> {
      logList.add(stringBuilder.toString());
    });
  }

  public void divideIncomingDateIntoLists(RequestReply reply) {
    for (Data data: reply.getResponseData()) {
      if (data.getCountry().equals("Germany")) {
        germanChartController.addDate(data);
      } else if (data.getCountry().equals("the Netherlands")) {
        dutchChartController.addDate(data);
      } else if (data.getCountry().equals("Belgium")) {
        belgiumChartController.addDate(data);
      }
    }
  }

  public void appendErrorToLog(Exception exception) {
    StringBuilder stringBuilder = new StringBuilder("");

    stringBuilder.append("Error: ");
    stringBuilder.append(exception.getMessage());
    stringBuilder.append("\n");

    Platform.runLater(() -> {
      logList.add(stringBuilder.toString());
    });
  }
}
