package app.domain;

import java.util.ArrayList;

public class RequestReply {
  private String corrId;
  private RequestFilters requestFilters;
  private ArrayList<Data> responseData;

  public RequestReply() {

  }

  public String getCorrId() {
    return corrId;
  }

  public void setCorrId(String corrId) {
    this.corrId = corrId;
  }

  public RequestFilters getRequestFilters() {
    return requestFilters;
  }

  public void setRequestFilters(RequestFilters requestFilters) {
    this.requestFilters = requestFilters;
  }

  public ArrayList<Data> getResponseData() {
    return responseData;
  }

  public void setResponseData(ArrayList<Data> responseData) {
    this.responseData = responseData;
  }
}
