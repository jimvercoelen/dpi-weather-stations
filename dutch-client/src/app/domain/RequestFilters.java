package app.domain;

public class RequestFilters {
  private boolean getGermany;
  private boolean getTheNetherlands;
  private boolean getBelgium;
  private String fromDate;
  private String tillDate;

  public RequestFilters() {

  }

  public RequestFilters(boolean getGermany, boolean getTheNetherlands, boolean getBelgium, String fromDate, String tillDate) {
    this.getGermany = getGermany;
    this.getTheNetherlands = getTheNetherlands;
    this.getBelgium = getBelgium;
    this.fromDate = fromDate;
    this.tillDate = tillDate;
  }

  public boolean isGetGermany() {
    return getGermany;
  }

  public void setGetGermany(boolean getGermany) {
    this.getGermany = getGermany;
  }

  public boolean isGetTheNetherlands() {
    return getTheNetherlands;
  }

  public void setGetTheNetherlands(boolean getTheNetherlands) {
    this.getTheNetherlands = getTheNetherlands;
  }

  public boolean isGetBelgium() {
    return getBelgium;
  }

  public void setgetBelgium(boolean getBelgium) {
    this.getBelgium = getBelgium;
  }

  public String getFromDate() {
    return fromDate;
  }

  public void setFromDate(String fromDate) {
    this.fromDate = fromDate;
  }

  public String getTillDate() {
    return tillDate;
  }

  public void setTillDate(String tillDate) {
    this.tillDate = tillDate;
  }
}
