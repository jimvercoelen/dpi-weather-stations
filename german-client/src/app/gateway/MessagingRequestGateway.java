package app.gateway;

import app.domain.RequestReply;
import com.google.gson.Gson;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

public class MessagingRequestGateway {


  private Connection connection;
  private Channel channel;

  private String host = "localhost";
  private String requestQueueName;
  private String replyQueueName;

  public MessagingRequestGateway(String host, String requestQueueName) throws IOException, TimeoutException {
    this.host = host;
    this.requestQueueName = requestQueueName;

    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(host);

    connection = factory.newConnection();
    channel = connection.createChannel();

    replyQueueName = channel.queueDeclare().getQueue();
  }

  public String requestData(RequestReply data) {
    final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

    AMQP.BasicProperties props = new AMQP.BasicProperties
      .Builder()
      .correlationId(data.getCorrId())
      .replyTo(replyQueueName)
      .build();

    try {
      String json = new Gson().toJson(data, RequestReply.class);

      channel.basicPublish("", requestQueueName, props, json.getBytes("UTF-8"));

      channel.basicConsume(replyQueueName, true, new DefaultConsumer(channel) {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
          if (properties.getCorrelationId().equals(data.getCorrId())) {
            response.offer(new String(body, "UTF-8"));
          }
        }
      });

      return response.take();

    } catch (IOException | InterruptedException e) {
      e.printStackTrace();

      return null;
    } finally {
      if (connection != null) {
        try {
          connection.close();
        } catch (IOException _ignore) { }
      }
    }
  }
}
