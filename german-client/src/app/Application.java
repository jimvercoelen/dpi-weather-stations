package app;

import app.controller.Controller;
import app.domain.RequestReply;
import app.gateway.MessagingRequestGateway;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Application extends javafx.application.Application {
  private final String HOST = "localhost";
  private final String TITLE = "German client";
  private final String EUROPEAN_REQUEST_CHANNEL = "european-request-weather-channel";
  private Controller controller;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws Exception {
    FXMLLoader loader = new FXMLLoader(getClass().getResource("view/view.fxml"));
    Pane root = loader.load();
    Scene scene = new Scene(root);

    stage.setTitle(TITLE);
    stage.setOnCloseRequest(event -> onApplicationShutDown());
    stage.setScene(scene);
    stage.show();

    controller = loader.getController();
    controller.setApplication(this);
  }

  public String requestData(RequestReply data) throws IOException, TimeoutException {
    MessagingRequestGateway requestGateway = new MessagingRequestGateway(HOST, EUROPEAN_REQUEST_CHANNEL);

    return requestGateway.requestData(data);
  }

  private void onApplicationShutDown() {
    // Empty
  }
}
