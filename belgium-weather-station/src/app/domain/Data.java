package app.domain;

import org.joda.time.DateTime;

public class Data {
  private final String country = "Belgium";
  private int temperature;
  private int humidity;
  private String dateTime;

  public Data() {
    this.dateTime = DateTime.now().toString();
  }

  public Data(int temperature, int humidity) {
    this.temperature = temperature;
    this.humidity = humidity;
    this.dateTime = DateTime.now().toString();
  }

  public String getCountry() {
    return country;
  }

  public int getTemperature() {
    return temperature;
  }

  public void setTemperature(int temperature) {
    this.temperature = temperature;
  }

  public int getHumidity() {
    return humidity;
  }

  public void setHumidity(int humidity) {
    this.humidity = humidity;
  }

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }

  @Override
  public String toString() {
    return "Country: " + country +
      " temperature: " + temperature
      + " humidity: " + humidity
      + "dateTime: " + dateTime;
  }
}
